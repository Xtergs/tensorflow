﻿import cv2
import sys
from imageai.Detection import VideoObjectDetection
import os
from matplotlib import pyplot as plt
import argparse

execution_path = os.getcwd()
color_index = {  'bicycle': 'olivedrab', 'person': 'honeydew'}
resized = False

total = {'bicycle': 0, 'person': 0}
frames_total = 0


def forFrame(frame_number, output_array, output_count, returned_frame):

    plt.clf()
    this_colors = []
    labels = []
    sizes = []
    totalSizes = []
    totalSizesLabels = []
    totalSizesColors = []

    counter = 0
    global frames_total
    frames_total += 1

    for eachItem in output_count:
        counter += 1
        labels.append(eachItem + " = " + str(output_count[eachItem]))
        sizes.append(output_count[eachItem])
        this_colors.append(color_index[eachItem])

    for eachItem in output_array:
        total[eachItem['name']] += 1
        if (eachItem['name'] == 'bicycle'):
            total['person'] -= 1
    

    for eachItem in total:
        x = total.get(eachItem)
        totalSizes.append(x)
        totalSizesLabels.append(eachItem + "=" + str(x))
        totalSizesColors.append(color_index[eachItem])

    global resized

    if (resized == False):
        manager = plt.get_current_fig_manager()
        manager.resize(width=1000, height=500)
        resized = True

    plt.subplot(1, 3, 1)
    plt.title("Frame : " + str(frame_number))
    plt.axis("off")
    plt.imshow(returned_frame, interpolation="none")

    plt.subplot(1, 3, 2)
    plt.title("Frame: " + str(frame_number))
    plt.pie(sizes, labels=labels, colors=this_colors, shadow=False, startangle=140, autopct="%1.1f%%")

    plt.subplot(1, 3, 3)
    plt.title("Total")
    plt.pie(totalSizes, labels=totalSizesLabels, colors=totalSizesColors, shadow=False, startangle=140, autopct="%1.1f%%")

    plt.pause(0.0001)

def outputfinal():
    plt.clf()
    plt.show()
    totalSizes = []
    totalSizesLabels = []
    totalSizesColors = []


    for eachItem in total:
        x = total.get(eachItem)
        totalSizes.append(x)
        totalSizesLabels.append(eachItem + "=" + str(x))
        totalSizesColors.append(color_index[eachItem])

    #plt.subplot(1, 1, 1)
    plt.title("Result. Frames=" + str(frames_total))
    plt.pie(totalSizes, labels=totalSizesLabels, colors=totalSizesColors, shadow=False, startangle=140, autopct="%1.1f%%")
    plt.pause(10000)


frames_total = 0
parser = argparse.ArgumentParser(description='Process some integers.')
parser.add_argument("--input_file", dest="input_file")
parser.add_argument("--input_dir", dest="input_dir2")
parser.add_argument("--output_dir", dest="output_dir2")
args = parser.parse_args()
camera = cv2.VideoCapture(args.input_file) 

detector = VideoObjectDetection()
detector.setModelTypeAsYOLOv3()
detector.setModelPath(os.path.join(execution_path , "yolo.h5"))
detector.loadModel()

custom = detector.CustomObjects(person=True, bicycle=True)
plt.show()
video_path = detector.detectCustomObjectsFromVideo(custom_objects=custom, camera_input=camera,
                                                   save_detected_video=False,
                                                   frames_per_second=30, 
                                                   log_progress=True, 
                                                   return_detected_frame=True, 
                                                   per_frame_function=forFrame)
detected_total = total['bicycle']*3 + total['person']
bicycle_persent = (total['bicycle']/detected_total) * 100
print ("Bicycle: " + str( bicycle_persent))
print ("Persons: " + str( (total['person']/detected_total) * 100))
print ("Bicycles per frame = " + str( (total['bicycle'] *3/frames_total)))
print ("Persons per frame = " + str( (total['person']/frames_total)))

if (bicycle_persent > 20 or (total['bicycle'] *3/frames_total) > 1 ):
    print ("We do recommend add separate road for bicycle!")
else:
    print ("We can't recommend you to add separate road for bicycle. However you can make them happy by adding the road :)")

outputfinal()


