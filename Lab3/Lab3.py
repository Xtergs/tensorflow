﻿import numpy as np
import sys
import os
import tensorflow as tf

###################################################################
# Variables                                                       #
# When launching project or scripts from Visual Studio,           #
# input_dir and output_dir are passed as arguments automatically. #
# Users could set them from the project setting page.             #
###################################################################

#FLAGS = tf.app.flags.FLAGS
#tf.app.flags.DEFINE_string("input_dir", ".", "Input directory where training dataset and meta data are saved")
#tf.app.flags.DEFINE_string("output_dir", ".", "Output directory where output such as logs are saved.")
#tf.app.flags.DEFINE_string("log_dir", ".", "Model directory where final model files are saved.")

def read_signals(filename):
    with open(filename, 'r') as fp:
        data = fp.read().splitlines()
        data = map(lambda x: x.rstrip().lstrip().split(), data)
        data = [list(map(float, line)) for line in data]
        data = np.array(data, dtype=np.float32)
    return data

def read_labels(filename):        
    with open(filename, 'r') as fp:
        activities = fp.read().splitlines()
        activities = list(map(int, activities))
    return np.array(activities)

def randomize(dataset, labels):
    permutation = np.random.permutation(labels.shape[0])
    shuffled_dataset = dataset[permutation, :, :]
    shuffled_labels = labels[permutation]
    return shuffled_dataset, shuffled_labels

def one_hot_encode(np_array, num_labels):
    return (np.arange(num_labels) == np_array[:,None]).astype(np.float32)

def reformat_data(dataset, labels):
    no_labels = len(np.unique(labels))
    labels = one_hot_encode(labels, no_labels)
    dataset, labels = randomize(dataset, labels)
    return dataset, labels

####

INPUT_FOLDER_TRAIN = './UCI_HAR/train/Inertial Signals/'
INPUT_FOLDER_TEST = './UCI_HAR/test/Inertial Signals/'

INPUT_FILES_TRAIN = ['body_acc_x_train.txt', 'body_acc_y_train.txt', 'body_acc_z_train.txt', 
                     'body_gyro_x_train.txt', 'body_gyro_y_train.txt', 'body_gyro_z_train.txt',
                     'total_acc_x_train.txt', 'total_acc_y_train.txt', 'total_acc_z_train.txt']

INPUT_FILES_TEST = ['body_acc_x_test.txt', 'body_acc_y_test.txt', 'body_acc_z_test.txt', 
                     'body_gyro_x_test.txt', 'body_gyro_y_test.txt', 'body_gyro_z_test.txt',
                     'total_acc_x_test.txt', 'total_acc_y_test.txt', 'total_acc_z_test.txt']

#####
print (os.getcwd())
print(os.path.isdir(INPUT_FOLDER_TRAIN))
train_signals, test_signals = [], []
for input_file in INPUT_FILES_TRAIN:
    signal = read_signals(INPUT_FOLDER_TRAIN + input_file)
    train_signals.append(signal)
train_signals = np.transpose(np.array(train_signals), (1, 2, 0))

for input_file in INPUT_FILES_TEST:
    signal = read_signals(INPUT_FOLDER_TEST + input_file)
    test_signals.append(signal)
test_signals = np.transpose(np.array(test_signals), (1, 2, 0))


def rnn_model(data, num_hidden, num_labels):
    splitted_data = tf.unstack(data, axis=1)
    
    cell = tf.nn.rnn_cell.BasicRNNCell(num_hidden)
 
    outputs, current_state = tf.nn.static_rnn(cell, splitted_data, dtype=tf.float32)
    output = outputs[-1]
    
    w_softmax = tf.Variable(tf.truncated_normal([num_hidden, num_labels]))
    b_softmax = tf.Variable(tf.random_normal([num_labels]))
    logit = tf.matmul(output, w_softmax) + b_softmax
    return logit

def rnn_lstm_model(data, num_hidden, num_labels):
    splitted_data = tf.unstack(data, axis=1)
    
    cell = tf.nn.rnn_cell.BasicLSTMCell(num_hidden)

    outputs, current_state = tf.nn.static_rnn(cell, splitted_data, dtype=tf.float32)
    output = outputs[-1]
    
    w_softmax = tf.Variable(tf.truncated_normal([num_hidden, num_labels]))
    b_softmax = tf.Variable(tf.random_normal([num_labels]))
    logit = tf.matmul(output, w_softmax) + b_softmax
    return logit

#####

LABELFILE_TRAIN = './UCI_HAR/train/y_train.txt'
LABELFILE_TEST = './UCI_HAR/test/y_test.txt'
train_labels = read_labels(LABELFILE_TRAIN)
test_labels = read_labels(LABELFILE_TEST)

#####

train_dataset, train_labels = reformat_data(train_signals, train_labels)
test_dataset, test_labels = reformat_data(test_signals, test_labels)

def main(_):
    num_units = 50
    signal_length = 128
    num_components = 9
    num_labels = 6

    num_hidden = 34
    learning_rate = 0.0001
    lambda_loss = 0.001
    total_steps = 10000
    display_step = 500
    batch_size = 1000

    def accuracy(y_predicted, y):
        return (100.0 * np.sum(np.argmax(y_predicted, 1) == np.argmax(y, 1)) / y_predicted.shape[0])

    ####

    graph = tf.Graph()
    with graph.as_default():
        #1) First we put the input data in a tensorflow friendly form.    
        tf_dataset = tf.placeholder(tf.float32, shape=(None, signal_length, num_components))
        tf_labels = tf.placeholder(tf.float32, shape = (None, num_labels))

        #2) Then we choose the model to calculate the logits (predicted labels)
        # We can choose from several models:
        logits = rnn_lstm_model(tf_dataset, num_hidden, num_labels)
        #logits = lstm_rnn_model(tf_dataset, num_hidden, num_labels)
        #logits = bidirectional_lstm_rnn_model(tf_dataset, num_hidden, num_labels)
        #logits = twolayer_lstm_rnn_model(tf_dataset, num_hidden, num_labels)
        #logits = gru_rnn_model(tf_dataset, num_hidden, num_labels)

        #3) Then we compute the softmax cross entropy between the logits and the (actual) labels
        l2 = lambda_loss * sum(tf.nn.l2_loss(tf_var) for tf_var in tf.trainable_variables())
        loss = tf.reduce_mean(tf.nn.softmax_cross_entropy_with_logits(logits=logits, labels=tf_labels)) + l2

        #4. 
        # The optimizer is used to calculate the gradients of the loss function 
        optimizer = tf.train.AdamOptimizer(learning_rate).minimize(loss)
        #optimizer = tf.train.GradientDescentOptimizer(learning_rate).minimize(loss)
        #optimizer = tf.train.AdagradOptimizer(learning_rate).minimize(loss)

        # Predictions for the training, validation, and test data.
        prediction = tf.nn.softmax(logits)

    with tf.Session(graph=graph) as session:
        tf.global_variables_initializer().run()
        print("\nInitialized")
        for step in range(total_steps):
            #Since we are using stochastic gradient descent, we are selecting  small batches from the training dataset,
            #and training the convolutional neural network each time with a batch. 
            offset = (step * batch_size) % (train_labels.shape[0] - batch_size)
            batch_data = train_dataset[offset:(offset + batch_size), :, :]
            batch_labels = train_labels[offset:(offset + batch_size), :]

            feed_dict = {tf_dataset : batch_data, tf_labels : batch_labels}
            _, l, train_predictions = session.run([optimizer, loss, prediction], feed_dict=feed_dict)
            train_accuracy = accuracy(train_predictions, batch_labels)

            if step % display_step == 0:
                feed_dict = {tf_dataset : test_dataset, tf_labels : test_labels}
                _, test_predictions = session.run([loss, prediction], feed_dict=feed_dict)
                test_accuracy = accuracy(test_predictions, test_labels)
                message = "step {:04d} : loss is {:06.2f}, accuracy on training set {} %, accuracy on test set {:02.2f} %".format(step, l, train_accuracy, test_accuracy)
                print(message)
    exit(0)


if __name__ == "__main__":
    tf.app.run()
