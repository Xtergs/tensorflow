import tensorflow as tf

graph = tf.get_default_graph()
x1Val = int(input("Input x1: "))
x2Val = int(input("Input x2: "))
x1 = tf.constant(x1Val)
x2 = tf.constant(x2Val)

result = tf.multiply(x1, x2)

session = tf.Session()

init = tf.initialize_all_variables()

session.run(init)
session_result = session.run(result)

print('{} * {} = {}'.format(session.run(x1), session.run(x2), session_result))