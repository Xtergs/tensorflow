import numpy as np
import tensorflow as tf

tf.reset_default_graph()

x = np.linspace(0.0, 10.0, num=100, dtype=float)
y = np.sin(x) + np.random.normal(size=len(x))

learning_rate = 0.001
n_epochs = 100

train_indexes = np.random.choice(list(range(len(x))), 3 * len(x) // 4)
test_indexes = np.array(range(len(x)))
test_indexes = np.delete(test_indexes, train_indexes)

x_train = x[train_indexes]
y_train = y[train_indexes]

x_test = x[test_indexes]
y_test = y[test_indexes]

with tf.name_scope("linear"):
    x_tf = tf.placeholder(shape=[None, 1], dtype=tf.float32)
    y_tf = tf.placeholder(shape=[None, 1], dtype=tf.float32)

    log = tf.placeholder(name="log", dtype=tf.float32)
    tf.summary.scalar("regression", log)
    write_op = tf.summary.merge_all()

    model_output = tf.Variable(tf.random_normal([1]), name="bias") + tf.Variable(tf.random_normal([1]), name="k") * x_tf

    loss = tf.reduce_mean(tf.pow(y_tf - model_output, 2))
    summary_loss_train = tf.summary.scalar("loss_train", loss)

    gd = tf.train.GradientDescentOptimizer(learning_rate)
    train_step = gd.minimize(loss)

    sess = tf.Session()
    sess.run(tf.global_variables_initializer())

    train_errors = []

    with sess:
        merge = tf.summary.merge_all()
        train_writer = tf.summary.FileWriter('./logs/linear/train', sess.graph)

        for i in range(len(y)):
            summary = sess.run(write_op, feed_dict={log: y[i]})
            train_writer.add_summary(summary, i)

        for i in range(n_epochs):
            _, train_err, summary_train = sess.run([train_step, loss, summary_loss_train],
                                                   feed_dict={x_tf: x_train.reshape((len(x_train), 1)),
                                                              y_tf: y_train.reshape((len(y_train), 1))})

            train_writer.add_summary(summary_train, i)

        regression_writer = tf.summary.FileWriter('./logs/linear/test', sess.graph)

        for i in range(len(x)):
            regression_output = sess.run(model_output, feed_dict={x_tf: x[i].reshape(1, 1)})
            summary = sess.run(write_op, feed_dict={log: regression_output[0][0]})
            regression_writer.add_summary(summary, i)

tf.reset_default_graph()

with tf.name_scope("non_linear"):
    x_tf = tf.placeholder(shape=[None, 1], dtype=tf.float32)
    y_tf = tf.placeholder(shape=[None, 1], dtype=tf.float32)

    log = tf.placeholder(name="log", dtype=tf.float32)
    tf.summary.scalar("regression", log)
    write_op = tf.summary.merge_all()

    model_output = tf.Variable(tf.random_normal([1]), name="a") + tf.multiply(tf.Variable(tf.random_normal([1]),
                                                                                          name="b"), tf.sqrt(x_tf))

    loss = tf.reduce_mean(tf.pow(y_tf - model_output, 2))
    summary_loss_train = tf.summary.scalar("non_linear_loss_train", loss)

    gd = tf.train.GradientDescentOptimizer(learning_rate)
    train_step = gd.minimize(loss)

    sess = tf.Session()
    sess.run(tf.global_variables_initializer())

    train_errors = []

    with sess:
        merge = tf.summary.merge_all()
        train_writer = tf.summary.FileWriter('./logs/non_linear/train', sess.graph)

        for i in range(len(y)):
            summary = sess.run(write_op, feed_dict={log: y[i]})
            train_writer.add_summary(summary, i)

        for i in range(n_epochs):
            _, train_err, summary_train = sess.run([train_step, loss, summary_loss_train],
                                                   feed_dict={x_tf: x_train.reshape((len(x_train), 1)),
                                                              y_tf: y_train.reshape((len(y_train), 1))})

            train_writer.add_summary(summary_train, i)

        regression_writer = tf.summary.FileWriter('./logs/non_linear/test', sess.graph)

        for i in range(len(x)):
            regression_output = sess.run(model_output, feed_dict={x_tf: x[i].reshape(1, 1)})
            summary = sess.run(write_op, feed_dict={log: regression_output[0][0]})
            regression_writer.add_summary(summary, i)

tf.reset_default_graph()

x = np.linspace(-10.0, 40.0, num=1000, dtype=float)
y = np.tanh(np.random.normal(size=len(x)))

y_train = y[train_indexes]

with tf.name_scope("logistic"):
    x_tf = tf.placeholder(shape=[None, 1], dtype=tf.float32)
    y_tf = tf.placeholder(shape=[None, 1], dtype=tf.float32)

    log = tf.placeholder(name="log", dtype=tf.float32)
    tf.summary.scalar("regression", log)
    write_op = tf.summary.merge_all()

    model_output = tf.Variable(1.0, name="bias") * tf.tanh(x_tf, name=None) * tf.Variable(tf.random_normal([1]),
                                                                                          name="k")

    loss = tf.reduce_mean(tf.pow(y_tf - model_output, 2))
    summary_loss_train = tf.summary.scalar("logistic_loss_train", loss)

    gd = tf.train.GradientDescentOptimizer(learning_rate)
    train_step = gd.minimize(loss)

    sess = tf.Session()
    sess.run(tf.global_variables_initializer())

    train_errors = []

    with sess:
        merge = tf.summary.merge_all()
        train_writer = tf.summary.FileWriter('./logs/logistic/train', sess.graph)

        for i in range(len(y)):
            summary = sess.run(write_op, feed_dict={log: y[i]})
            train_writer.add_summary(summary, i)

        for i in range(n_epochs):
            _, train_err, summary_train = sess.run([train_step, loss, summary_loss_train],
                                                   feed_dict={x_tf: x_train.reshape((len(x_train), 1)),
                                                              y_tf: y_train.reshape((len(y_train), 1))})

            train_writer.add_summary(summary_train, i)

        regression_writer = tf.summary.FileWriter('./logs/logistic/test', sess.graph)

        for i in range(len(x)):
            regression_output = sess.run(model_output, feed_dict={x_tf: x[i].reshape(1, 1)})
            summary = sess.run(write_op, feed_dict={log: regression_output[0][0]})
            regression_writer.add_summary(summary, i)
