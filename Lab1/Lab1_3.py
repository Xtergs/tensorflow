import tensorflow as tf

graph = tf.get_default_graph()

x = tf.constant(1.0, name='input')
w = tf.Variable(0.98, name='weight')
y = tf.multiply(x, w, name='output')

target = tf.constant(0.0, name='target')
loss = tf.pow(y - target, 2, name='loss')
train_step = tf.train.GradientDescentOptimizer(0.025).minimize(loss)

for value in [x,w, y, target, loss]:
    tf.summary.scalar(value.op.name, value)

summaries = tf.summary.merge_all()

session = tf.Session()
summary_writer = tf.summary.FileWriter('log_stats', session.graph)

session.run(tf.initialize_all_variables())
for i in range(100):
    summary_writer.add_summary(session.run(summaries), i)
    session.run(train_step)
session.close()